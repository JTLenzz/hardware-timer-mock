/*
 * Copyright (c) 2014, Jonathan Lenz
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY THE EARLIER MENTIONED AUTHORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <copyright holder> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "gmock/gmock.h"
#include "MockHardwareTimer.h"
#include <cstdint>

using namespace ::testing;

class HardwareTimer : public Test
{
public:
   MockHardwareTimer<uint16_t> timer_;
};

TEST_F(HardwareTimer, initialCountIsZero)
{
   ASSERT_THAT(timer_.Count(), Eq(0));
}

TEST_F(HardwareTimer, initialOverflowValueIsMaximumValue)
{
   ASSERT_THAT(timer_.OverflowValue(), Eq(0xFFFF));
}

class HardwareTimerTiming : public Test
{
public:
   unsigned frequency_;
   MockHardwareTimer<uint16_t> timer_;
   
   HardwareTimerTiming() : frequency_{16000000}, timer_(frequency_) { }
};

TEST_F(HardwareTimerTiming, correctNumberOfTicksPerMillisecond)
{
   timer_.RunFor(1000);
   ASSERT_THAT(timer_.Count(), Eq(frequency_ / 1000));
}

TEST_F(HardwareTimerTiming, correctNumberOfTicksPerMicrosecond)
{
   timer_.RunFor(1);
   ASSERT_THAT(timer_.Count(), Eq(frequency_ / 1000 / 1000));
}
