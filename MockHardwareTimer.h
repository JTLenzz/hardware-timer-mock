/*
 * Copyright (c) 2014, Jonathan Lenz
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY THE EARLIER MENTIONED AUTHORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <copyright holder> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MOCKHARDWARETIMER_H_
#define MOCKHARDWARETIMER_H_

#include <limits>

template<typename RegType>
class MockHardwareTimer
{
public:
   MockHardwareTimer() : MockHardwareTimer(DefaultFrequency_) { }
   MockHardwareTimer(unsigned frequency) : frequency_(frequency), count_(0) { }
   
   virtual ~MockHardwareTimer() { }
   
   RegType Count() const
   {
      return count_;
   }
   
   RegType OverflowValue() const
   {
      return std::numeric_limits<RegType>::max();
   }
   
   void RunFor(unsigned microseconds)
   {
      count_ += (frequency_ / 1000 / 1000) * microseconds;
   }
   
private:
   static const unsigned DefaultFrequency_ = 1000000;
   
   unsigned frequency_;
   RegType count_;
};

#endif /* MOCKHARDWARETIMER_H_ */
